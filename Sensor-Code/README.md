# **EEE3088F_Proj**

## **Sensor-Code**
For this project, a variety of sensors will be used to collect analog and digital data. As a result, the code to be uploaded onto the microcontroller will be rather large in size. Hence, this branch will be used to edit the portion of the code related to the sensing, as it is much safer to edit this code separately from the main code, where even small errors may lead to larger issues. In addition to the code, the pricelist and datasheets of various sensors will be uploaded to this branch.
