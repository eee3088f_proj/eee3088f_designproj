# **EEE3088F_Proj**
A joint repository for a third year engineering design project

## **Members**
The three members working on this engineering project are:
* **Michael Hills** - Power Management
* **Zuhayr Loonat** - Microcontroller Interfacing and Coding
* **Md Shaihan Islam** - Sensing

## **Overview**
The design project to be designed comprises of an enviro-sensing **HAT** (**H**ardware **A**ttached on **T**op). The HAT will consist of a number of sensors, namely light, temperature and humidity, which will connect to process and store the measured data in an ordered and organised way. This is useful for a variety of purposes involving the climate of small areas, as it measures three of the primary influences on climate. Examples of where such techbology would be ideal, include greenhouse control, solar panel installation and various construction processes such as testing the viability of a certain cement in a certain climate. It is also ideal for weather enthusiasts and climate investigation.

## Various branches in the repository
* [Code](https://gitlab.com/eee3088f_proj/eee3088f_designproj/-/tree/main/Code) - In this branch, majority of the code to be uploaded on the microcontroller will be posted.
* [Documentation](https://gitlab.com/eee3088f_proj/eee3088f_designproj/-/tree/main/Documentation) - In this branch, important documentation such as requirements, specifications, etc. will be posted.
* [PCB](https://gitlab.com/eee3088f_proj/eee3088f_designproj/-/tree/main/PCB) - In this branch, the various PCB designs and their gerber files will be uploaded and updated.  
* [Power](https://gitlab.com/eee3088f_proj/eee3088f_designproj/-/tree/main/Power) - In this branch, all files related to power management of the project, such as schematics, will be uploaded and reviewed.
* [Sensor-Code](https://gitlab.com/eee3088f_proj/eee3088f_designproj/-/tree/main/Senspr-Code) - This branch will contain all the versions of code and schematics regarding the sensors that the project will utilise.
* [Simulations](https://gitlab.com/eee3088f_proj/eee3088f_designproj/-/tree/main/Simulations) - This branch will contain the updated versions of the results of the various circuit simulations that will be undertaken on LTSpice.

Please note that the above links are directed to the introductory page of the respective branch, which provides more information regarding the selected branch.

# Contents of this README 
1. [User hardware requirements](#userrequirements)
2. [User software and tools requirements](#softwaretools)
3. [Connecting the hardware](#hardwareconnect)
4. [Using the basic program to use the features of the PCB](#programrun)
5. [Licence used in the repository](#licencerepo)

## User hardware requirements <a name="userrequirements"></a>


## User software and tools requirements <a name="softwaretools"></a>


## Connecting the hardware <a name="hardwareconnect"></a>


## Using the basic program to use the features of the PCB <a name="programrun"></a>


## Licence used in the repository <a name="licencerepo"></a>