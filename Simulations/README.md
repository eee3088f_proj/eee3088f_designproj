# **EEE3088F_Proj**

## **Simulations**
It is always important to test a design before implementing it. Simulation software such as LTSpice enables the opportunity to virtually test any cirucit design, as well as graph and compare different input and output signals. In this branch, graphs of input and output signals will be stored after the simulation software testing has been completed. The various circuits that have been, or will be simulated will also be uploaded onto this branch.
