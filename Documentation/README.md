# **EEE3088F_Proj**
## **Documentation**
In this branch, all the important documents that ensures that the project is able to run smoothly and that any issues are able to be solved as soon as it is identified. Such documents include: requirements, specifications, time schedule, components pricelist, etc. The documents will be uploaded and updated as the project develops. Each document will also be uploaded in a '.docx' format (for editing) as well as a '.pdf' format (for general viewing). 

Click the following link to access the LucidChart page used to design the block diagram: [Block Diagram Link](https://lucid.app/lucidchart/6f4155c0-dd41-4df6-9df6-a29792932397/edit?view_items=edxAOlSKGf1Z&invitationId=inv_f93d24f2-7113-4dc7-8a55-13d2d90bb092)
