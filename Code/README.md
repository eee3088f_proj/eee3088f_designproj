# **EEE3088F_Proj**

## **Code**
Welcome to the coding branch! In this branch, majority of the code to be uploaded on the microcontroller will be posted. The microcontroller to be used to control the entire system is the **STMicroelectronics STM32F051C6 Microcontroller**. This RISC (Reduced Intsruction Set Computing) microcontroller is based on the **ARM Cortex<sup>TM</sup> - M0** microchip. The microcontroller will be coded in the C language, and all versions of the code that will be written will be uploaded in this branch, in both '.c' and '.txt' formats. 
